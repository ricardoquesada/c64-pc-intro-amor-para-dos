;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Flashparty 2021 - 256-bytes intro
; x86 part
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;
; LIA: http://lia.rebelion.digital/
; code: riq (http://retro.moe)
;
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;

bits  16
;cpu   8086
org   0x100

section .text

start:
        ; A "Dual x86-c64" could be done in different ways.
        ; For example, using autorun, and starting a address "0x02cc".
        ; Some demo parties require that C64 must start at 0x0801.

        ; Assumes: BX=0x0000, SI=0x0100, CX=0x??FF
        add     [bx+si], cx                     ; Bytes: 0x01, 0x08: C64 loading address: $0801
        or      cx, [bx+si]                     ; Bytes: 0x0b, 0x0b: Next BASIC instruction: $080b
        jnz     x86_start                       ; Jump to our real start address.
                                                ; "Z" is cleared from prev instruction.
                                                ; These 2 bytes in the C64 are not imporant.
                                                ; Represent the line number of the SYS.
        db      0x9e                            ; BASIC "SYS" opcode.
        db      0x32, 0x32, 0x32, 0x34          ; "2224" or $08b0. Start address for the C64.
        db      0x00                            ; End of line.
        db      0x00, 0x00                      ; End of BASIC program.

        ; Assumes:
        ; AX=0, BX=0, CX=0x0?FF
x86_start:
        mov     al,0x13
        int     0x10                            ; Set 320x160x256 graphics mode

.pal_loop:
        mov     ax,cx
        mov     dx,0x3c8
        out     dx,al                           ; Select palette color
        inc     dx
        shl     al,2                            ; Red * 4, generates 4 "banded" rings
        out     dx,al                           ; Red
        mov     ax,cx
;        shl     al,1
        out     dx,al                           ; Green
;        mov     ax,cx
        shl     al,1
        out     dx,al                           ; Blue
        loop    .pal_loop

        ; Globals:
        ; BP=timer
        ; CL=align X, CH=align Y

        ; Values for the two positions:
        ; A) CH=100, CL=0
        ; B) CH=225, CL=128
        ; It should be possible to save one byte by starting with 1st effect first,
        ; but I like the second effect better.
        ; By changing ES=0xa000-10 it should be possible to start with
        ; second effect first, and saving one byte, but it didn't look as good.
        mov     cx,225 * 256 + 128              ; Align Y & X


        ; "Framework" taken from Memories
        ; http://www.sizecoding.org/wiki/Memories

        push    0xa000                          ; write the screen adress to register ES
        pop     es                              ; works in conjunction with Rrrola trick

        mov     ax,0x251c                       ; parameter for changing timer interrupt
        mov     dx,timer                        ; adress of timer routine, assume DH=1
        int     0x21                            ; install timer routine


.l0:
        mov     ax,0xcccd                       ; Magic Rrrola constant
        mul     di

        mov     byte [start],ah                 ; Used for the sound, but is not very reliable
                                                ; since this value changes multipe times before
                                                ; the sound uses it. But reliable-enough for our needs.

        ; Similar to "Memories" Circle effect with some changes:
        ;  changed some offsets
        ;  changed palette
        ;  tweeked the formula a bit
        ; ...and that's it.
        mov     al,dh                           ; get Y in AL
        sub     al,ch                           ; align Y vertically
        imul    al                              ; AL = Y²
        xchg    dx,ax                           ; Y²/256 in DH, X in AL
        sub     al,cl                           ; align X horizontally
        imul    al                              ; AL = X²
        sub     dh,ah                           ; DH = (X² - Y²)/257
        mov     al,dh                           ; AL = (X² + Y²)/256
        add     ax,bp                           ; offset color by time

;        and     al,0x2f                         ; Use "pastel" palette
;        or      al,0x80

        stosb

        in      al,0x60                         ; check for ESC
        dec     al
        jnz     .l0                             ; no? repeat

        ret                                     ; exit

timer:
        inc     bp                              ; timer++

        ; In theory AX should be saved, but the worst thing that can happen is
        ; that some pixels aren't displayed correcty, so it is 'safe' to overwrite AX
        ;push    ax

        cmp     bp,180                          ; elapsed 180 ticks? ~9 seconds
        jbe     .l0                             ; no

        ; Changing the offsets gets triggered at the middle of the
        ; rendering, causing the "split" effect.
        xor     cx,(225^100) * 256 + (128^0)    ; New alignments for X & Y
        cmp     bp,246                          ; From 180 to 246, keep switching the aligments
        jbe     .l0                             ; and stop at 246.
        sub     bp,bp                           ; Reset timer to start again

.l0:
        mov     ax,bp                           ; Sound is a combination of timer + pseudo random
        shr     ax,1
        jnc     .skip
        xor     al,byte [start]                 ; use "random" to amplify/reduce the distance
                                                ; between lo and hi notes
.skip:  or      al,0x20                         ; Make sure all notes are have a low pitch
        out     0x42,al
        ;nop                                    ; Needed?
        out     0x42,al
        mov     al,0x4b
        out     61h,al

        ;pop     ax
        iret

;lfsr_constant: dw    0x00f2
buffer:
        ; At most, the x86 program uses 126 bytes
        ; The rest are reserved for the C64
        times 126-($-start)  db ' '

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
        ; C64 program starts here
        ; Address: 0x087D