.PHONY: dos c64

all: dual

dos:
	@echo "*** Generating DOS binary ***"
	nasm -Ox -Wall dos/main.asm -fbin -o bin/dos.com

c64:
	@echo "*** Generating C64 binary ***"
	64tass -Wall -Werror --cbm-prg -o bin/c64.prg -L bin/list.txt -l bin/labels.txt --vice-labels c64/main.asm

dual: c64 dos
	@echo "*** Generating C64+DOS binary ***"
	@# Remove the first two bytes of the .prg since they contain
	@# the start address
	dd bs=1 skip=2 if=./bin/c64.prg of=./bin/c64-raw.bin

	@# Append
	cat ./bin/dos.com ./bin/c64-raw.bin > ./bin/amorxdos.com

rundos: dual
	dosbox bin/amorxdos.com

rundosx: dual
	dosbox-x bin/amorxdos.com

rundoss: dual
	~/src/dosbox/dosbox-staging/build/dosbox bin/amorxdos.com

runc64: dual
	x64sc -verbose -moncommands bin/labels.txt bin/amorxdos.com

clean:
	-rm bin/*.bin bin/*.com bin/*.prg bin/*.txt
